# Instagram cover picker

Simple POC for instagram styled cover picker

## Technologies

From experience, I found that expo modules are most useful for critical logic, lika audio, video and interacting with file system, so this project is a bare RN app with expo modules added. Main modules are:

- expo-av - for video display (here used only for reading duration)
- expo-file-system - for caching remote video
- expo-video-thumbnails - to extract thumbnails from the video

## Logic

For simplicity I did not care about the proper folder structure, client state management, linting. Also used plain JS, but prefer Typescript for production apps. The idea is to first load video in device cache, then read a duration, and then generate a set of screenshots. While video is caching, I show the progress bar with percentage. Number of screenshots per video and video url itself can be changed in `app/api/constants.js`.
