import * as VideoThumbnails from 'expo-video-thumbnails';

export const generateScreenshots = async (
  videoUrl,
  screenshotsNum,
  duration,
) => {
  const step = duration / screenshotsNum;
  try {
    return await Promise.all(
      Array(screenshotsNum)
        .fill('')
        .map(async (_, i) => {
          try {
            const {uri} = await VideoThumbnails.getThumbnailAsync(videoUrl, {
              time: i * step,
            });
            return uri;
          } catch (e) {
            throw new Error(e);
          }
        }),
    );
  } catch (e) {
    throw new Error(e);
  }
};
