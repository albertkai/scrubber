// Here we use reanimated and gesture handler to handle animations and gestures. This are better
// solutions for production app as animations run on a separate thread and do not block the UI.
// This allows for a complex animations and interactions. The downside is that in the development mode
// hot reload does not update the UI thread. The other downside is mostly silent bugs on UI thread. But
// the performance boost of the UI thread gives a bigger benefit.
// In real app I'd move the animation and gesture logic into a hook.

import React from 'react';
import styled from 'styled-components';
import {GestureDetector, Gesture} from 'react-native-gesture-handler';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';
import {Dimensions} from 'react-native';

const Scrubber = ({
  screenshots,
  selectedScreenshotIndex,
  setSelectedScreenshotIndex,
  progress,
}) => {
  const isPressed = useSharedValue(false);
  const offsetX = useSharedValue(0);
  const startX = useSharedValue(0);

  const animatedStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {translateX: offsetX.value},
        {scale: withSpring(isPressed.value ? 1.2 : 1)},
      ],
      width: withSpring(isPressed.value ? 50 : 0),
      opacity: withSpring(isPressed.value ? 1 : 0),
    };
  });

  const gesture = Gesture.Pan()
    .runOnJS(true)
    .onBegin(e => {
      isPressed.value = true;
      startX.value = e.x;
      offsetX.value = e.x;
    })
    .onUpdate(e => {
      const panPosition = e.translationX + startX.value;
      offsetX.value = panPosition;
      const updatedIndex = Math.floor(
        panPosition / (Dimensions.get('window').width / screenshots.length),
      );
      if (updatedIndex !== selectedScreenshotIndex) {
        setSelectedScreenshotIndex(updatedIndex);
      }
    })
    .onEnd(() => {
      startX.value = offsetX.value;
    })
    .onFinalize(() => {
      isPressed.value = false;
    });

  if (!screenshots.length) {
    return (
      <Container>
        <Progress progress={progress} />
        <ProgressText>{Math.floor(progress * 100)}%</ProgressText>
      </Container>
    );
  }
  return (
    <GestureDetector gesture={gesture}>
      <Container>
        {screenshots.map(screenshot => (
          <Segment
            key={screenshot}
            source={{url: screenshot}}
            length={screenshot.length}
          />
        ))}
        <Handle
          style={animatedStyles}
          source={{uri: screenshots[selectedScreenshotIndex]}}
        />
      </Container>
    </GestureDetector>
  );
};

const Container = styled.View`
  position: relative;
  width: 100%;
  height: 100px;
  flex-direction: row;
  background-color: rgba(255, 255, 255, 0.1);
`;

const Handle = styled(Animated.Image).attrs({
  resizeMode: 'cover',
})`
  position: absolute;
  height: 100%;
  width: 30px;
  border-radius: 10px;
`;

const Segment = styled.Image.attrs({
  resizeMode: 'cover',
})`
  flex-grow: 1;
  background-color: red;
`;

const Progress = styled.View`
  position: absolute;
  width: ${p => 100 * p.progress}%;
  height: 100%;
  top: 0;
  left: 0;
  background: rgba(255, 255, 255, 0.2);
`;

const ProgressText = styled.Text`
  color: white;
  font-size: 30px;
  font-weight: 200;
  top: 8px;
  left: 8px;
`;

export default Scrubber;
