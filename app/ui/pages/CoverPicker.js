// Video can take a long time to load, thats why I decided to go with filesystem in order to show progress.
// So the flow is: first we load a video to the cache, then read length in the Video component (unfortunately expo-av does not provide video api)
// and onLoad event of the Video component we generate the screenshots and show the UI.
// In the real world app it will make sense to keep the logic in a separate hook to make component smaller.

import React, {useState, useRef, useEffect} from 'react';
import styled from 'styled-components/native';
import {ActivityIndicator} from 'react-native';
import {Video} from 'expo-av';
import * as FileSystem from 'expo-file-system';

import Scrubber from '../components/Scrubber';
import {DEFAULT_SCREENSHOTS_LENGTH, VIDEO_URL} from '../../api/constants';
import {generateScreenshots} from '../../api/helpers';

const CoverPicker = () => {
  // In production I'd rather use redux to handle this state, as probably this state will
  // be used in another logic, like server mutations etc, but for the POC i went with component state.
  const [selectedScreenshotIndex, setSelectedScreenshotIndex] = useState(0);
  const [progress, setProgress] = useState(0);
  const [localVideoUrl, setLocalVideoUrl] = useState(null);
  const [isPreparing, setIsPreparing] = useState(true);
  const [screenshots, setScreenshots] = useState([]);
  const [error, setError] = useState(null);
  const downloadResumable = useRef(
    FileSystem.createDownloadResumable(
      VIDEO_URL,
      FileSystem.documentDirectory + 'example.mp4',
      {},
      downloadProgress => {
        setProgress(
          downloadProgress.totalBytesWritten /
            downloadProgress.totalBytesExpectedToWrite,
        );
      },
    ),
  );

  useEffect(() => {
    loadVideo();
  }, []);

  const loadVideo = async () => {
    const {uri} = await downloadResumable.current.downloadAsync();
    setLocalVideoUrl(uri);
  };

  const onVideoLoad = ({durationMillis}) => {
    init(durationMillis);
  };

  const init = async duration => {
    try {
      const generatedScreenshots = await generateScreenshots(
        localVideoUrl,
        DEFAULT_SCREENSHOTS_LENGTH,
        duration,
      );
      setScreenshots(generatedScreenshots);
      setIsPreparing(false);
    } catch (e) {
      console.log(e);
      setError(e);
      setIsPreparing(false);
    }
  };

  const renderScreenshot = () => {
    if (isPreparing) {
      return <ActivityIndicator color="white" />;
    } else if (error) {
      return <ErrorText>Error ocurred</ErrorText>;
    }
    return <Screenshot source={{uri: screenshots[selectedScreenshotIndex]}} />;
  };

  return (
    <Container>
      <Header>Choose cover</Header>
      <ScreenshotContainer>{renderScreenshot()}</ScreenshotContainer>
      {localVideoUrl && (
        <HiddenVideo
          source={{
            uri: localVideoUrl,
          }}
          onLoad={onVideoLoad}
        />
      )}
      {!error && (
        <Scrubber
          screenshots={screenshots}
          selectedScreenshotIndex={selectedScreenshotIndex}
          setSelectedScreenshotIndex={setSelectedScreenshotIndex}
          progress={progress}
        />
      )}
    </Container>
  );
};

const Container = styled.SafeAreaView`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.9);
  align-items: center;
  justify-content: space-around;
`;

const Header = styled.Text`
  color: white;
  font-size: 22px;
  font-weight: bold;
`;

const Screenshot = styled.Image.attrs({
  resizeMode: 'cover',
})`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  border-radius: 10px;
`;

const ScreenshotContainer = styled.View`
  position: relative;
  height: 320px;
  width: 200px;
  align-items: center;
  justify-content: center;
  background-color: rgba(255, 255, 255, 0.1);
  border-radius: 10px;
`;

const ErrorText = styled.Text`
  color: red;
`;

const HiddenVideo = styled(Video)`
  position: absolute;
  top: -2000px;
  left: -2000px;
  width: 0;
  height: 0;
  opacity: 0;
`;

export default CoverPicker;
